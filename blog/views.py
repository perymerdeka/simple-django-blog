from django.views import generic

# import models
from .models import Posts


# Create your views here.

class PostsListView(generic.ListView):
    queryset = Posts.objects.filter(status=1).order_by('created_on')
    template_name = 'blog/index.html'


class PostsDetailView(generic.DetailView):
    model = Posts
    template_name = 'blog/posts_detail.html'

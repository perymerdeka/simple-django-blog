from django.contrib import admin
from blog.models import Posts

#  config panel admin for posts model
class PostsAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'status', 'created_on')
    list_filter = ("status", )
    search_fields = ['title', 'contents']
    prepopulated_fields = {'slug': ('title', )}

# Register your models here.
admin.site.register(Posts, PostsAdmin)
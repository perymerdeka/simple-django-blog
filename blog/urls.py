from . import views
from django.urls import path

# posts urls route

urlpatterns = [
    path('', views.PostsListView.as_view(), name='posts'),
    path('<slug:slug>/', views.PostsDetailView.as_view(), name='posts_detail'),

]

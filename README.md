### Simple Django Blog Projects

---

simple django blog is a project for learning django 

### How To Run This Projects

* **Linux/Mac**

create virtualenv with this command

```bash
python3 -m venv venv
```

for linux or mac activate virtual environtment with these command

```commandline
source venv/bin/activate
```

* **For Windows**

create virtualenv with these command

```bash
virtualenv venv
```

activate with

```bash
venv/Scripts/activate.bat
```

#### Install requirements

```bash
pip install -r requirements.txt --upgrade pip
```

#### Run

```bash
python manage.py createsuperuser --username="admin" --email="admin@django.id"
```
then fill the password with `admin` then press `y`

```bash
python manage.py makemigrations
```

```bash
python manage.py migrate
```

```bash
python manage.py runserver
```

try open http://localhost:8000/